<?php

namespace Drupal\query\Services;

use Drupal\query\Common\Condition;

interface QueryInterface {
    public function condition(?string $key = NULL) : Condition;
}
