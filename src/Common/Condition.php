<?php

namespace Drupal\query\Common;

class Condition {

    private string $key;

    /**
     * The conjunction to use for the whole group of requirement groups.
     */
    private string $conjunction = Conjunction::TYPE_AND;

    /**
     * @var ConditionRequirementGroup[]
     */
    private array $requirementGroups;

    public function __construct(string|null $key = NULL)
    {
        $this->key = $key;
    }

    public static function create(string|null $key = NULL): static
    {
        return new static($key);
    }

    public function getGroupConjunction(): string
    {
        return $this->conjunction;
    }

    /**
     * @return ConditionRequirementGroup[]
     */
    public function getRequirementGroups(): array
    {
        return $this->requirementGroups;
    }

    private function getRequirementGroup(string $conjunction = Conjunction::TYPE_AND): ConditionRequirementGroup
    {
        $current = current($this->requirementGroups);
        if (NULL === $current) {
            $this->requirementGroups[] = ConditionRequirementGroup::create($conjunction);
        }
        return current($this->requirementGroups);
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): static
    {
        $this->key = $key;
        return $this;
    }

    public function key(string $key): static
    {
        return $this->setKey($key);
    }

    public function isNull(): static
    {
        $this->getRequirementGroup()->append(Expression::isNull());
        return $this;
    }

    public function isEmpty(): static
    {
        $this->getRequirementGroup()->append(Expression::isEmpty());
        return $this;
    }

    public function isNotEmpty(): static
    {
        $this->getRequirementGroup()->append(Expression::isNotEmpty());
        return $this;
    }

    public function is(bool $strict = FALSE): static
    {
        return $strict ? $this->isEqualTo(TRUE) : $this->isEquivalentTo(TRUE);
    }

    public function isEquivalentTo(string $value): static
    {
        $this->getRequirementGroup()->append(Expression::isEquivalentTo($value));
        return $this;
    }

    public function isEqualTo(string $value): static
    {
        $this->getRequirementGroup()->append(Expression::isEqualTo($value));
        return $this;
    }

    public function isNotEquivalentTo(string $value): static
    {
        $this->getRequirementGroup()->append(Expression::isNotEquivalentTo($value));
        return $this;
    }

    public function isNotEqualTo(string $value): static
    {
        $this->getRequirementGroup()->append(Expression::isNotEqualTo($value));
        return $this;
    }

    public function isIn(array $value): static
    {
        $this->getRequirementGroup()->append(Expression::isIn($value));
        return $this;
    }

    public function hasAllOf(array|string $mixed): static
    {
        $array = is_array($mixed) ? $mixed : [$mixed];
        $this->getRequirementGroup()->append(Expression::hasAllOf($array));
        return $this;
    }

    public function hasNoneOf(array|string $mixed): static
    {
        $array = is_array($mixed) ? $mixed : [$mixed];
        $this->getRequirementGroup()->append(Expression::hasNoneOf($array));
        return $this;
    }

    public function isNotIn(array $value): static
    {
        $this->getRequirementGroup()->append(Expression::isNotIn($value));
        return $this;
    }

    public function isGreaterThan(string $string): static
    {
        $this->getRequirementGroup()->append(Expression::isGreaterThan($string));
        return $this;
    }

    public function isGreaterThanOrEqualTo(string $string): static
    {
        $this->getRequirementGroup()
            ->append(Expression::isGreaterThanOrEqualTo($string));
        return $this;
    }

    public function isLessThan(string $string): static
    {
        $this->getRequirementGroup()->append(Expression::isLessThan($string));
        return $this;
    }

    public function isLessThanOrEqualTo(string $string): static
    {
        $this->getRequirementGroup()
            ->append(Expression::isLessThanOrEqualTo($string));
        return $this;
    }

    public function isBetween(string $a, string $b): static
    {
        $this->getRequirementGroup()->append(Expression::isBetween($a, $b));
        return $this;
    }

    public function isOutside(string $a, string $b): static
    {
        $this->getRequirementGroup()->append(Expression::isOutside($a, $b));
        return $this;
    }
}
