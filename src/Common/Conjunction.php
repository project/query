<?php

namespace Drupal\query\Common;

class Conjunction {
    public const TYPE_AND = 'and';
    public const TYPE_OR = 'or';
}
