<?php

namespace Drupal\query\Common;

class ConditionRequirementGroup {

    private string $conjunction;

    private array $requirements = [];

    public function __construct(string $conjunction)
    {
        $this->conjunction = $conjunction;
    }

    public static function create(string $conjunction = Conjunction::TYPE_AND): static
    {
        return new static($conjunction);
    }

    public function getConjunction(): string
    {
        return $this->conjunction;
    }

    /**
     * @return Expression[]
     */
    public function getRequirements(): array
    {
        return $this->requirements;
    }

    public function append(Expression $expression): static
    {
        $this->requirements[] = $expression;
        return $this;
    }
}
