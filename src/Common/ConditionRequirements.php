<?php

namespace Drupal\query\Common;

class ConditionRequirements {

    private string $conjunction = Conjunction::TYPE_AND;

    /**
     * @var Expression[]
     */
    private array $requirements = [];

    public function getConjunction(): string
    {
        return $this->conjunction;
    }

    public function setConjunction(string $conjunction): static
    {
        $this->conjunction = $conjunction;
        return $this;
    }

    public function append(Expression $expression): static
    {
        if (!empty($this->requirements)) {
            $this->requirements[] = $this->conjunction;
        }
        $this->requirements[] = $expression;
        return $this;
    }
}
