<?php

namespace Drupal\query\Common;

class Expression {

    private string $operator;

    /**
     * One or more values to compare against.
     */
    private array|string|null $mixed;

    /**
     * Optional 2-digit language code for the condition.
     */
    private string $language_code;

    /**
     * Condition constructor.
     *
     * @param $operator
     *   The operator to use in the condition.
     * @param $values
     *   One or more values to compare against.
     * @param $language_code
     *   Optional 2-digit language code for the condition.
     */
    public function __construct(string $operator, array|string|null $values = NULL, string $language_code = NULL)
    {
        $this->operator = $operator;
        $this->mixed = $values;
        $this->language_code = $language_code;
    }

    public function getRaw(): array
    {
        return [
            'operator' => $this->operator,
            'values' => $this->mixed,
            'language_code' => $this->language_code,
        ];
    }

    public static function isNull(): Expression
    {
        return new static(Operator::TYPE_NULL);
    }

    public static function isEmpty(): Expression
    {
        return new static(Operator::TYPE_EMPTY);
    }

    public static function isNotEmpty(): Expression
    {
        return new static(Operator::TYPE_NOT_EMPTY);
    }

    /**
     * Builds an 'equivalent' expression.
     */
    public static function isEquivalentTo(string $string): static
    {
        return new static(Operator::TYPE_EQUIVALENT, $string);
    }

    /**
     * Builds an 'equals' expression.
     */
    public static function isEqualTo(string $string): static
    {
        return new static(Operator::TYPE_EQUALS, $string);
    }

    public static function isNotEquivalentTo(string $string): Expression
    {
        return new static(Operator::TYPE_NOT_EQUIVALENT, $string);
    }

    public static function isNotEqualTo(string $string): Expression
    {
        return new static(Operator::TYPE_NOT_EQUALS, $string);
    }

    public static function isIn(array $array): Expression
    {
        return new static(Operator::TYPE_IN, $array);
    }

    public static function isNotIn(array $array): Expression
    {
        return new static(Operator::TYPE_NOT_IN, $array);
    }

    /**
     * Whether the collection in context has ALL the given value(s).
     */
    public static function hasAllOf(array|string $mixed): Expression
    {
        $array = is_array($mixed) ? $mixed : [$mixed];
        return new static(Operator::TYPE_HAS, $array);
    }

    /**
     * Whether the collection in context has NONE of the given value(s).
     */
    public static function hasNoneOf(array|string $mixed): Expression
    {
        $array = is_array($mixed) ? $mixed : [$mixed];
        return new static(Operator::TYPE_HAS_NOT, $array);
    }

    public static function isGreaterThan(string $string): Expression
    {
        return new static(Operator::TYPE_GREATER_THAN, $string);
    }

    public static function isGreaterThanOrEqualTo(string $string): Expression
    {
        return new static(Operator::TYPE_GREATER_THAN_EQUAL_TO, $string);
    }

    public static function isLessThan(string $string): Expression
    {
        return new static(Operator::TYPE_LESS_THAN, $string);
    }

    public static function isLessThanOrEqualTo(string $string): Expression
    {
        return new static(Operator::TYPE_LESS_THAN_EQUAL_TO, $string);
    }

    public static function isBetween(string $a, string $b): Expression
    {
        return new static(Operator::TYPE_BETWEEN, $a, $b);
    }

    public static function isOutside(string $a, string $b): Expression
    {
        return new static(Operator::TYPE_OUTSIDE, $a, $b);
    }

    /**
     * @return bool
     *   TRUE if $a relates to $b according to the given operator.
     *   TRUE if $a relates to $b & $c according to the given operator.
     */
    public static function evaluate(string $a, string $operator, string $b, string $c = NULL): bool
    {
        switch ($operator) {
            case Operator::TYPE_BETWEEN:
                return $b < $a && $a < $c;

            case Operator::TYPE_OUTSIDE:
                return $a < $b && $c < $a;
        }

        if (NULL !== $c) {
            throw new \LogicException(vsprintf('Argument C not compatible with operator: %s', [
                $operator,
            ]));
        }

        return match ($operator) {
            Operator::TYPE_EQUALS => $a == $b,
            Operator::TYPE_NOT_EQUALS => $a != $b,
            Operator::TYPE_GREATER_THAN => $a > $b,
            Operator::TYPE_LESS_THAN => $a < $b,
            Operator::TYPE_GREATER_THAN_EQUAL_TO => $a >= $b,
            Operator::TYPE_LESS_THAN_EQUAL_TO => $a <= $b,
            default => throw new \DomainException(sprintf('Unknown operator: %s', $operator)),
        };
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * Gets one or more values to compare against.
     */
    public function getValues(): array|string|null
    {
        return $this->mixed;
    }

    /**
     * Gets one value from the defined values.
     */
    public function getValue(int $offset = 0): array|string|null
    {
        return is_array($this->mixed) ? $this->mixed[$offset] : $this->mixed;
    }

    /**
     * The 2-digit language code for the condition, if any.
     */
    public function getLanguageCode(): string|null
    {
        return $this->language_code;
    }
}
