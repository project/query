<?php

namespace Drupal\query\Common;

class Operator {
    public const TYPE_NULL = 'null';
    public const TYPE_NOT_NULL = 'not_null';
    public const TYPE_EMPTY = 'empty';
    public const TYPE_NOT_EMPTY = 'not_empty';
    public const TYPE_EQUIVALENT = 'equivalent';
    public const TYPE_EQUALS = 'equals';
    public const TYPE_NOT_EQUALS = 'not_equals';
    public const TYPE_NOT_EQUIVALENT = 'not_equivalent';
    public const TYPE_IN = 'in';
    public const TYPE_NOT_IN = 'not_in';
    public const TYPE_HAS = 'has';
    public const TYPE_HAS_NOT = 'has_not';

    public const TYPE_BETWEEN = 'between';
    public const TYPE_OUTSIDE = 'outside';

    public const TYPE_GREATER_THAN = 'greater_than';
    public const TYPE_GREATER_THAN_EQUAL_TO = 'greater_than_equal_to';
    public const TYPE_LESS_THAN = 'less_than';
    public const TYPE_LESS_THAN_EQUAL_TO = 'less_than_equal_to';
}
